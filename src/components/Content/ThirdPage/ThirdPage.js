import React,{Component} from 'react';
import Post from '../../Post/Post';
import DataPost from '../../Data_Post/Data_Post';
import aixos from 'axios';
import {Button} from 'react-bootstrap';
import ReactTimeout from 'react-timeout'


class ThirdPage extends Component{

    state={
        list:[],

    }

    componentDidMount(){
        aixos.get("/todos/")
            .then(response=>{
                this.setState({
                    list:response.data.slice(0,5)
                });
            })
            .catch(err =>{
                console.log("error")
            });       
        // setInterval( () => this.componentDidMount(), 5000 );
        // console.log("bottom")
    }

    showAlert=(id)=>{
        alert(id)
    }


    changePg=(event)=>{
        this.props.mvPage();
    }

    render(){
        const post=this.state.list.map (posts =>{
            return <div className="col-12 col-sm-6 col-md-3">
                       <Post name={posts.title} num={posts.id}  clicked={(event)=>this.showAlert(posts.id)} />
                   </div>
        })
        return(
            <div>
                <h2>Title</h2>
                <div className="row">
                    {post}
                </div>
                <div>
                <DataPost/>
                </div>
                <Button bsStyle="success" onClick={(event)=>this.changePg(event)}>Next</Button><br/>
            </div>
        );


    }




}

export default ThirdPage;