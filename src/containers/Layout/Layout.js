import React, {Component} from 'react';
import Content from '../../components/Content/Content/Content';
import Footer from '../../components/Footer/Footer';
import Head from '../../components/Header/Header';
import Sidebar from '../Sidebar/Sidebar';

// import classes from './Layout.css';


  class Layout extends Component{ 
      
    state={
        main:'one'
    }

    render(){
        return(
        <div>
            <Head property={this.state.main}/>
            <Content/>  
            <main>
                {this.props.children}
            </main>
            <Footer/>
        </div>
        );
    }
}

export default Layout;